# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

'''@file fibcreator.py
'''

def fib(idx):
    
    ## @brief This function returns the fibonacci number for a specified index.
    #  @param idx is the user specified index that they want to find the fibonacci number for.
    #  @author: Jameson Spitz
    #  @Date: 09/27/21
    ##

    
    fibo = [0,1]                 # Initializing fibonacci list to add to in loop
    i = 2                        # Used to index fibonacci list
    
    if(int(idx) == 1):
        return 1
    if(int(idx) ==0):
        return 0
    
    while True:                  # Loop that continually finds the next fibonacci number. 
                                       
        x = fibo[i-1] + fibo[i-2]
        fibo.append(x)
        
        if i == int(idx):        # Breaks when the fibonacci index matches the user input index
            break
        
        i += 1
    
    return fibo[int(idx)]

## Calling this section to be ran in main. Will be the user interaction point

if __name__ == '__main__':

    while True:
        
        idx = input('Please enter an index of the fibonacci number you want: ')    
       
       # Loop to prompt user to enter a new number until their input is valid (positive integer)
       
        while True:
            try:
                int(idx)
            except:
                idx = input('Oops! Please enter an integer only: ')
                continue
                
            if (int(idx) < 0):
                idx = input('Make sure you enter a positive number!: ')
                continue
            else:
                break               
            
        # check to see if user wants to find another fibonaci number
        
        print ('Fibonacci number at index {:} is {:}.'.format(idx,fib(idx)))
        again = input('Would you like to play again? (y/n): ')
            
        if (again.lower() == 'n'):
            print('Thanks for playing!')
            break
